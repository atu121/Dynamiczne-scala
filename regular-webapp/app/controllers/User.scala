package controllers

import java.sql.{PreparedStatement, ResultSet, SQLException, Statement}

import javax.inject.Inject
import models.Tranings
import play.api.cache._
import play.api.data.Form
import play.api.data.Forms.{mapping, _}
import play.api.db.Database
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json._
import play.api.mvc.{Action, Controller, _}

import scala.collection.mutable.ListBuffer


class User @Inject()(db: Database, cache: CacheApi, val messagesApi: MessagesApi) extends Controller with I18nSupport {
  def AuthenticatedAction(f: Request[AnyContent] => Result): Action[AnyContent] = {
    Action { request =>
      (request.session.get("id").flatMap { id =>
        cache.get[JsValue](id + "profile")
      } map { profile =>
        f(request)
      }).orElse {
        Some(Redirect(routes.Application.login()))
      }.get
    }
  }

  def index = AuthenticatedAction { request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
    Ok(views.html.user(profile))
  }


  def adminPanel = AuthenticatedAction { request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
       if(profile.asInstanceOf[JsObject].value("name").asInstanceOf[JsString].value.equals("test1@gmail.com")){
         Ok(views.html.addTraning(tranForm))
       }else{
    Ok(views.html.accessDeniedUser())
       }
  }

  val tranForm = Form(
    mapping(
      "id" -> number,
      "data" -> text,
      "name" -> text,
      "time" -> text,
      "price" -> text
    )(Tranings.apply)(Tranings.unapply)
  )

  def tranings = AuthenticatedAction { request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
    Ok(views.html.trainingList(profile, getTraning()))
  }

  def myTraning = AuthenticatedAction { request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
    val personId: Int = findIdByEmail(profile.asInstanceOf[JsObject].value("name").asInstanceOf[JsString].value)
    Ok(views.html.mySaveTraning(profile, getMyTraning(personId)))
  }

  def saveTraning(idTraning: Int) = AuthenticatedAction { request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
    val personId: Int = findIdByEmail(profile.asInstanceOf[JsObject].value("name").asInstanceOf[JsString].value)
    setTraining(personId, idTraning)
    Ok(views.html.mySaveTraning(profile, getMyTraning(personId)))
  }

  def cancelTraning(idTraning: Int) = AuthenticatedAction { request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
    val personId: Int = findIdByEmail(profile.asInstanceOf[JsObject].value("name").asInstanceOf[JsString].value)
    resignationTraning(idTraning)
    Ok(views.html.mySaveTraning(profile, getMyTraning(personId)))
  }


  def addTraning() = Action { implicit request =>
    val id = request.session.get("id").get
    val profile = cache.get[JsValue](id + "profile").get
    val name = request.body.asInstanceOf[AnyContentAsFormUrlEncoded].data.get("name").get(0)
    val data = request.body.asInstanceOf[AnyContentAsFormUrlEncoded].data.get("date").get(0)
    val time = request.body.asInstanceOf[AnyContentAsFormUrlEncoded].data.get("time").get(0)
    val price = request.body.asInstanceOf[AnyContentAsFormUrlEncoded].data.get("price").get(0)
    addNewTraning(name, data, time, price)
    Ok(views.html.trainingList(profile, getTraning()))
  }


  def getTraning(): List[Tranings] = {
    val tranings = ListBuffer[Tranings]()
    val conn = db.getConnection()
    try {
      val stmt = conn.createStatement
      val rs = stmt.executeQuery("SELECT * FROM trainings WHERE person_id IS NULL")
      while (rs.next()) {
        val id: Integer = rs.getInt("trainings_id")
        val data: String = rs.getString("data")
        val name: String = rs.getString("name")
        val time: String = rs.getString("time")
        val price: String = rs.getString("price")
        tranings += (new Tranings(id = id, data = data, name = name, time = time, price = price))

      }
    } finally {
      conn.close()
    }
    return tranings.toList
  }


  def setTraining(personId: Int, traningId: Int) = {
    val conn = db.getConnection()
    try {
      val stmt = conn.createStatement
      val rs = stmt.executeUpdate("UPDATE trainings SET person_id = " + personId + " WHERE trainings_id = " + traningId + " ")
    } finally {
      conn.close()
    }
  }

  def getMyTraning(personId: Int): List[Tranings] = {
    val tranings = ListBuffer[Tranings]()
    val conn = db.getConnection()
    try {
      val stmt = conn.createStatement
      val rs = stmt.executeQuery("Select * From trainings where person_id=  " + personId + " ")
      while (rs.next()) {
        val id: Integer = rs.getInt("trainings_id")
        val data: String = rs.getString("data")
        val name: String = rs.getString("name")
        val time: String = rs.getString("time")
        val price: String = rs.getString("price")
        tranings += (new Tranings(id = id, data = data, name = name, time = time, price = price))

      }
    } finally {
      conn.close()
    }
    return tranings.toList
  }

  def findIdByEmail(email: String): Int = {
    val conn = db.getConnection()
    var id: Integer = null
    try {
      val stmt = conn.createStatement
      val rs = stmt.executeQuery("Select person_id From persons where email=  '" + email + "' ")
      while (rs.next()) {
        id = rs.getInt(1)
      }
    } finally {
      conn.close()
    }
    if (id != null) {
      return id
    }
    else {
      return addEmail(email)
    }
  }

  def resignationTraning(traningId: Int) = {
    val conn = db.getConnection()
    try {
      val stmt = conn.createStatement
      val rs = stmt.executeUpdate("UPDATE trainings SET person_id = NULL WHERE trainings_id = " + traningId + " ")
    } finally {
      conn.close()
    }
  }


  def addNewTraning(name: String, data: String, time: String, price: String) = {
    val conn = db.getConnection()
    try {
      val stmt = conn.createStatement
      val rs = stmt.executeUpdate("INSERT INTO trainings (data, name, time, price) VALUES " +
        "( '" + data + "' ,  '" + name + "' ,  '" + time + " ' ,  '" + price + "' ) ")
    } finally {
      conn.close()
    }
  }


  def addEmail(email: String): Int = {
    var saveId: Integer = null
    val conn = db.getConnection()
    val statement: PreparedStatement = conn.prepareStatement("INSERT INTO persons (email) VALUES ('" + email + "') ",
      Statement.RETURN_GENERATED_KEYS)
    val affectedRows: Int = statement.executeUpdate
    if (affectedRows == 0) {
      throw new SQLException("Creating user failed, no rows affected.");
    }
    try {
      val generatedKeys: ResultSet = statement.getGeneratedKeys()

      if (generatedKeys.next()) {
        saveId = (generatedKeys.getInt(1))
        return saveId
      }
      else {
        throw new SQLException("Creating user failed, no ID obtained.");
      }
    } finally {
      conn.close()
    }
  }


}