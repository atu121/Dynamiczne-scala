CREATE TABLE persons (
    person_id  SERIAL PRIMARY KEY,
    email varchar(255) NOT NULL
);


CREATE TABLE trainings (
    trainings_id  SERIAL PRIMARY KEY,
    data varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    time varchar(255) NOT NULL,
    price numeric CONSTRAINT positive_price CHECK (price > 0),
    person_id integer REFERENCES persons (person_id)
);


INSERT INTO trainings (data, name, time, price)
VALUES ('14-12-2018 10:00:00', 'Boks', '1.5h', 50);

INSERT INTO trainings (data, name, time, price)
VALUES ('14-12-2018 12:00:00', 'K1', '1h', 55);

INSERT INTO trainings (data, name, time, price)
VALUES ('14-12-2018 14:00:00', 'Cross fit', '1.5h', 40);

INSERT INTO trainings (data, name, time, price)
VALUES ('14-12-2018 16:00:00', 'Zumba', '2h', 60);